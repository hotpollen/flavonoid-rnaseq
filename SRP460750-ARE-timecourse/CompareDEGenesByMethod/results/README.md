# README #

This folder contains results from data analysis performed with code in the
parent directory.

What's here:

* CvT-compare-[assembly].txt - where "assembly" is one of "SL4" or "SL5". These files report results from CompareDEGenesByMethod.Rmd, which compares results from using DESeq2 or edgeR to find differentially expressed genes between treatment and controls, within a single genotype or treatment duration. 

