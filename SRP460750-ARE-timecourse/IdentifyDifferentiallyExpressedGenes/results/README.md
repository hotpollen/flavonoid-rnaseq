# README #

This folder contains results from data analysis performed with code in the
parent directory.

What's here:

* CvT-[method]-[assembly].txt - where method is one of "DESeq2" and "edgeR" and assembly is one of "SL4" or "SL5". These were made by Markdown files named FindControVsStressDEGenes-[assembly]-[method].Rmd. These files report differentially expressed genes between treatment and controls, within a single genotype or treatment duration. Genes are from assembly SL4 or SL5.

* MvW-[assembly].txt and MvW.34.75-[assembly].txt - where "assembly" is one of "SL4" or "SL5." Files named MvW-[assembly].txt report differences in expression between genotypes _are_ and VF36, same temperature and same time point. These files are the output of FindMutantVsWildtypeDEGenes-DESeq2.Rmd.

* MvW-temp-[assembly].xlsx - output of FindTreatmentEffectAcrossGenotypes-DESeq2.Rmd, which finds differentially expressed genes between treatment (28C, 34C) and genotype (are & VF36). The excel files include a different sheet for each timepoint (15,30,45,75). The results are ordered by pvalue with the most significant at the top. The method used was DESeq2 and the output includes both assemblies SL4 & SL5.  
