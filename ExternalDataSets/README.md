# About this folder / directory

Everything here came from some other Web site.
Nothing here is created by any code in this repository.

* * *

## *.gtf

We made these file and are saving it here for the convenience of people
wanting to run nextflow, using genome assembly as indicated in the file name.

This files are derived from gene model bed-detail file downloaded from their
respective genome version files hosted at 
https://igbquickload-main.bioviz.org/quickload/H_exemplaris_Z151_Apr_2017 and
converted to gtf using code from https://bitbucket.org/lorainelab/genomesource/,
probably bedDetailToGeneInfo.py.

If the original data files gets changed or updated, this copy here
might also need to be updated.

* * *

## Looking for other files needed to run nextflow rnaseq pipeline?

These might include:

* a gene model bed12 file (only 12 columns, no gene name 13th column or description 14th column)
* a genome fasta file

You can find what you need by visiting the genome directory for your reference
assembly stored in the IGB ecosystem's subversion repository, deployed on various Quickload Main hosts.

For example, see: http://igbquickload-main.bioviz.org/quickload

To make a genome fasta (.fa) file, get the genome 2bit (.2bit) file and convert it using
2bitTofa.

To make a bed file, get the bed12 file (.bed.gz), uncompress it and remove the final two
columns using Unix cut command.
