# README #

This repository contains code and data files for managing, processing,
visualizing, and analyzing RNA-Seq data from NSF Plant Genome Research Project titled 
[Genomic analysis of heat stress tolerance during tomato pollination](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1939255).

Data are from RNA-Seq experiments investigating the role of flavonoid biosynthesis and
metabolism in fertilization, during pollen tube growth, created in the laboratory of Gloria Muday at Wake Forest University in North Carolina. 
