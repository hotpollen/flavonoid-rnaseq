#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2
#SBATCH --mem=8gb
#SBATCH --time=15:00:00
#SBATCH --partition=Orion

cd $SLURM_SUBMIT_DIR 
module load samtools
module load anaconda3/2020.11

# S,F should be defined in the call to sbatch 
# S - sample name
# F - file name; should be file.bam

# from https://deeptools.readthedocs.io/en/latest/content/tools/bamCoverage.html#usage-examples-for-rna-seq

opts="--binSize 1 --normalizeUsing CPM --outFileFormat bedgraph"

if [ ! -s $S.scaled.bedgraph.gz ]; then
    bamCoverage $opts -b $F -o $S.scaled.bedgraph
    bgzip $S.scaled.bedgraph
fi
if [ ! -s $S.scaled.bedgraph.gz.tbi ]; then
    tabix -s 1 -b 2 -e 3 $S.scaled.bedgraph.gz
fi
