#!/bin/bash

# to run: 
#    sbatch-doIt.sh [extension] [script] >jobs.out 2>jobs.err
# to kill:
#    cat jobs.out | xargs scancel 
#
# example)
#
#   sbatch-doIt.sh .sam samtools.sh >jobs.out 2>jobs.err
#

EXT=$1
SCRIPT=$2

FS=$(ls *${EXT})
for F in $FS
do
    S=$(basename $F ${EXT})
    #CMD="S=$S F=$F sbatch --export=S=$S --export=F=$F --job-name=$S --output=$S.out --error=$S.err $SCRIPT"
    CMD="sbatch --export=S=$S,F=$F --job-name=$S --output=$S.out --error=$S.err $SCRIPT"
    $CMD
done
