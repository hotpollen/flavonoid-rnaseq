#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2
#SBATCH --mem=8gb
#SBATCH --time=15:00:00
#SBATCH --partition=Orion

#
# This script uses a tool from NCBI called "prefetch" to download data
# files from the Sequence Research Archive in ".fastq" format. Next,
# it uses another tool called "vdb-validate" to make sure the download
# did not fail.
#
# You can run this in parallel, on a cluster, if the cluster nodes are
# able to access the internet. 
#
# If yes, then you can run it like this:
#
#    cat srr.txt | xargs -I A sbatch --export=S=A --job-name=A --output=A.out --error=A.err prefetch.sh
#
# where srr.txt is file with SRR run accessions, one per line.
#
# How it works:
#
#  The preceding line of code delivers SRR run accessions, one at a time, to the xargs
#  command. The xargs command then runs sbatch, passing an SRR run accession as variable
#  named A. Note how we chose the variable name "A" using the -I option. Also note
#  how we can refer to the value represented by variable "A" without using a $ symbol
#  prefix. It's a thing xargs can do, but hard to explain.
#
#  Observe that the "sbatch" command gets some options, too. It's final argument
#  is the name of this script. And, it's getting access to a shell variable named S,
#  that gets defined by the sbatch command via its --export option.
#
#  The code is passing a lot of stuff around, from cat to xargs to sbatch to prefetch. 
#
# Always check:
#
#  Did the download work as expected? How do you know? View the output files which are all named
#  after the SRR identifier. Check what the "validate" program wrote. 
#
#
# Other things to know:
#
# (1) This script assumes that the user has configured their account
#     (see: ~/.ncbi/user-settings.mkdf) to save output to the current working
#     directory. You can do this using a "vdb" command. See documentation link
#     below.
#
# (2) prefetch downloads run files as ".sra" format to a new folder named
#     for the SRR accession. It creates that new folder if not there already.
#
# (3) Why prefetch? Do this because if the download halts, prefetch can
#     resume re-download and save you some time and server power.
#
# For more info about prefetch, see this excellent documentation:
# https://github.com/ncbi/sra-tools/wiki/08.-prefetch-and-fasterq-dump
#
#
cd $SLURM_SUBMIT_DIR
module load sra-tools/2.11.0
CMD1="prefetch $S"
echo "Running: $CMD1"
$CMD1
CMD2="vdb-validate $S/$S.sra"
echo "Running: $CMD2"
$CMD2


